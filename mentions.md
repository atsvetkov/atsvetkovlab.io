[ASP.NET Community Standup - March 9th, 2017](https://www.youtube.com/watch?v=l3FTrnDd6yU#t=12m32s)

[The week in .NET - Feb 28, 2017](https://blogs.msdn.microsoft.com/dotnet/2017/02/28/the-week-in-net-on-net-with-eric-mellino-happy-birthday-from-scott-hunter-ozcode/)
[The week in .NET - Apr 30, 2017](https://blogs.msdn.microsoft.com/dotnet/2017/04/04/the-week-in-net-on-net-on-sonarlint-and-sonarqube-happy-birthday-net-with-dan-fernandez-nopcommerce-steve-gordon/)
[The week in .NET - May 30, 2017](https://blogs.msdn.microsoft.com/dotnet/2017/05/30/the-week-in-net-open-xml-sdk-adventure-time/)
[The week in .NET - August 15, 2017](https://blogs.msdn.microsoft.com/dotnet/2017/08/15/the-week-in-net-net-core-2-0-asp-net-core-2-0-entity-framework-2-0-visual-studio-2017-update-3-enterprise-entity-framework-core-in-boston-and-links/)

[C# Digest #149](https://csharpdigest.net/digests/149)
[C# Digest #157](https://csharpdigest.net/digests/157)

[ASP.NET Weekly - Issue #5](https://www.getrevue.co/profile/aspnetweekly/issues/asp-net-weekly-issue-5-70346)
[ASP.NET Weekly - Issue #19](https://www.getrevue.co/profile/aspnetweekly/issues/asp-net-weekly-issue-19-84659)