![Build Status](https://gitlab.com/pages/hugo/badges/master/build.svg)

---

Sources of my technical blog hosted at [https://surfingthecode.com](https://surfingthecode.com). Built with [Hugo](https://gohugo.io/) and [Beautiful Hugo](https://github.com/halogenica/beautifulhugo) theme.