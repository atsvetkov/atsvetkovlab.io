---
title: Alexander Tsvetkov
subtitle: 
comments: false
---

I have been building and architecting software systems with .NET, Python and Go for the last 14 years. Before becoming a computer geek, I studied solid-state physics and could have become one of those crazy scientists. Eventually got into programming and never looked back. My first computer was an Intel 80386 PC with no hard-drive, and I still vividly remember configuring extended memory in MS-DOS in order to start a game, or bringing *Doom II* to my classmates on a bunch of floppy disks.

When not programming at work, I am... programming for fun (*what did you expect from a geek?*), learning new technologies, and writing about it here. Occasionally, though, I also play the guitar, produce electronic music, play football, and enjoy a good coffee.

You can find my full CV on [LinkedIn](https://www.linkedin.com/in/alexandertsvetkov/) and some of my code on [GitHub](https://github.com/atsvetkov), like this [dotnet-rocks](https://github.com/atsvetkov/dotnet-rocks) tool.