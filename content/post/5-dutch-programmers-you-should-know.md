---
title: "5 Dutch programmers you should know"
date: 2020-02-29
tags: ["Dutch", "famous programmers", "computer science", "video games"]
---

February 2020 is kind of a milestone for me: it has been five years since I moved to the Netherlands. This country has a rich history of contributions to computer science, which now reflects in it having a vibrant IT market and lots of tech startups. I decided that a good way to celebrate this five-year milestone would be to look at five great Dutch minds who contributed immensely to how computers are used today. Even if you aren't a programmer (but especially if you are), chances are that you rely heavily on some of the tools and techniques invented by these people.

<!--more-->

### 1. Edsger Dijkstra

{{< figure src="/img/dijkstra.gif" title="" >}}

[Edsger W. Dijkstra](https://en.wikipedia.org/wiki/Edsger_W._Dijkstra) was born in 1930 in Rotterdam and is considered to be one of the most influential computer scientists of all times. He contributed to numerous fields, like compilers, operating systems, distributed systems, concurrent programming, and many more. He published more than 1300 papers and was also known for his unconventional approach to teaching and eloquence sometimes combined with rudeness (he once said, *"The question of whether Machines Can Think (…) is about as relevant as the question of whether Submarines Can Swim."*). Seriously, even just the single Wikipedia page about him is a great read.

I'm pretty sure every computer science student at some point encountered the famous [Dijkstra's algorithm](https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm) of finding the shortest path in a graph. It is deceptively simple, but serves as a foundation of many more advanced techniques, which are actually used in the modern navigation applications. Even if you didn't happen to study it at the university, most likely your smartphone used some very similar algorithm the last time you asked Google Maps for directions to a pub.

If you'd like a refresher on Dijkstra's and other graph algorithms, I would definitely recommend [_Graph Search, Shortest Paths, and Data Structures_](https://www.coursera.org/learn/algorithms-graphs-data-structures) (offered by Stanford and taught by [Tim Roughgarden](http://timroughgarden.org)) or [_Algorithms on Graphs_](https://www.coursera.org/learn/algorithms-on-graphs) (offered by University of California San Diego) on Coursera.

### 2. Mark Overmars

{{< figure src="/img/hld.gif" title="" >}}

Not to be confused with [Marc Overmars](https://en.wikipedia.org/wiki/Marc_Overmars), a great Ajax and Arsenal football player, [Mark Overmars](https://en.wikipedia.org/wiki/Mark_Overmars) is a computer scientist and teacher of game programming, who was born in Zeist and did his PhD at Utrecht University. You might not be familiar with his works in dynamic data structure design, computational geometry, and robotics, but if you are into video games, you might know his other creation - a cross-platform game engine [GameMaker](https://en.wikipedia.org/wiki/GameMaker_Studio). It was originally released in 1999, still exists, and was used to build such gems as [Hotline Miami](https://en.wikipedia.org/wiki/Hotline_Miami), [Hyper Light Drifter](https://en.wikipedia.org/wiki/Hyper_Light_Drifter), and [Gunpoint](https://en.wikipedia.org/wiki/Gunpoint_(video_game)) (see more on the [website of _YoYo Games_](https://www.yoyogames.com/showcase), the company that now develops the engine).

### 3. Guido van Rossum

{{< figure src="/img/python.gif" title="" >}}

According to the annual [StackOverflow Developer Survey results](https://insights.stackoverflow.com/survey/2019#most-loved-dreaded-and-wanted), Python is the 4th most popular, 2nd most loved, and 1st most wanted programming language in the world. It's easy to learn, very readable, has a great community and plenty of high-quality OSS libraries around. It has seen a surge in popularity during last years, probably caused by more and more businesses adopting machine learning technologies, where Python is especially popular. And this is the language that [Guido van Rossum](https://en.wikipedia.org/wiki/Guido_van_Rossum) has created about 30 years ago.

Guido studied computer science and mathematics in the University of Amsterdam, and during his time there he worked on (among other things) on [ABC programming language](https://en.wikipedia.org/wiki/ABC_(programming_language)), which eventually led him to creating Python. According to Guido himself, he *"meant to honor the irreverent comedic genius of [Monty Python's Flying Circus](https://en.wikipedia.org/wiki/Monty_Python%27s_Flying_Circus)"*, so the name is really more about the TV series than a snake. Guido worked at Google and Dropbox, all this time being Python's "Benevolent Dictator For Life", until stepping down from that role in 2018 and retiring in 2019. Despite his major contribution to the language, he always pointed out that Python was *"developed on the Internet, entirely in the open"* and that *"a programming language created by a community fosters happiness in its users around the world"* (see [the text of his "King's Day Speech"](http://neopythonic.blogspot.com/2016/04/kings-day-speech.html) given at a TED Talk in 2016).

If you'd like to hear a brief story of Python in Guido's own words, [here's a very short interview with him](https://www.youtube.com/watch?v=J0Aq44Pze-w) at Oracle Developers channel (there are also plenty of his more technical talks from various conferences on YouTube).

### 4. Bram Moolenaar

{{< figure src="/img/vim_on_fire_large.gif" title="" >}}

> “How do you generate a random string? Put a first year Computer Science student in Vim and ask them to save and exit.”

Another "Benevolent Dictator For Life", [Bram Moolenaar](https://en.wikipedia.org/wiki/Bram_Moolenaar) is a Dutch programmer and an active member of the OSS community, mostly known as the author of [Vim](https://en.wikipedia.org/wiki/Vim_(text_editor)) (**Vi IMproved**), the famous hard-to-exit text editor. Vim is *extremely* popular, despite having an *extremely* steep learning curve. It proposes a very different way of working with text by introducing 12 edit modes, which separate different categories of tasks, like navigating the text, selecting parts of it, running commands etc. The [commands](https://www.fprintf.net/vimCheatSheet.html) are the most powerful part, since they are flexible, provide endless combinations, and almost form a language of its own (Chris Toomey's [Mastering the Vim Language](https://www.youtube.com/watch?v=wlR5gYd6um0) talk gives a great explanation of this concept). Vim key bindings are quite peculiar and promote typing efficiency by keeping the fingers on the [home row](https://www.computerhope.com/jargon/h/hrk.htm). After getting used to this, it may be hard to go back to the mainstream way of navigating around - that's why almost all other popular text editors and IDEs have plugins for enabling the "VIM Mode", including [Visual Studio Code](https://marketplace.visualstudio.com/items?itemName=vscodevim.vim), [Visual Studio](https://marketplace.visualstudio.com/items?itemName=JaredParMSFT.VsVim), [Sublime](https://www.sublimetext.com/docs/3/vintage.html), [JetBrains IDEs](https://plugins.jetbrains.com/plugin/164-ideavim), and even [Emacs](https://www.emacswiki.org/emacs/Evil) (where the corresponding plugin is called "Evil" - perhaps, a manifestation of the eternal [editor war](https://en.wikipedia.org/wiki/Editor_war)).

Bram is also an advocate of the [International Child Care Fund Holland](https://en.wikipedia.org/wiki/ICCF_Holland), an organization trying to support children in Uganda. In fact, almost every time someone starts the Vim editor, they are encouraged to help.

{{< figure src="/img/vim_uganda.png" title="" >}}

### 5. Andries Brouwer

{{< figure src="/img/nethack.gif" title="" >}}

[Andries Evert Brouwer](https://en.wikipedia.org/wiki/Andries_Brouwer) is a Dutch matematician and computer programmer, who was also a professor at Eindhoven University of Technology for 26 years. One somewhat exotic contribution he is known for is a single-player roguelike video game called [NetHack](https://www.nethack.org/). The game was created in 1987 *and has been getting updates till this day* ([latest version 3.6.5 has been released on January 27, 2020](https://www.nethack.org/v365/release.html)). It features fancy ASCII graphics (a player is represented by a humble `@` character) and can be played in a terminal or... [via SSH](https://nethackwiki.com/wiki/Public_server)! Seriously, if you want to just try it out, you don't even need to install anything: open your favourite shell, run `ssh ssh://nethack@alt.org`, register a user - and start your ASCII adventure!

{{< figure src="/img/nethack.png" title="" >}}

Admittedly, NetHack is a very niche product, but, for those true hackers who are really into it, it is [the greatest game you will ever play](http://thegreatestgameyouwilleverplay.com/).
