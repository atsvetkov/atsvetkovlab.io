---
title: "Coffee and Links #5"
date: 2017-02-16
tags: ["coffee and links", ".net", "visual studio", ".net core", "ndc", "ndc london"]
aliases: ["/2017/07/coffee-and-links-5/"]
---

It's time for a new coffee and some new links! This time it is mostly about .NET, since there are important news and updates.

<!--more-->

{{< figure src="/img/coffee-and-links-5.jpg" title="" >}}

### 15 years of .NET

Just three days ago [.NET got 15 years old](https://blogs.msdn.microsoft.com/dotnet/2017/02/13/happy-15th-birthday-net/)! It is almost unbeliveable. I started my software development career somewhere in 2005, when I joined a small outsourcing company in St. Petersbug. I had zero knowledge of C# and ASP.NET (we were using .NET 1.1 back then and could only dream about generics), so most likely I was producing the most horrible code ever written... But I was a quick learner too, so after these 12 years the code I write has hopefully become slightly less awful. Today's .NET Framework is a very different place to be in, especially because of the open-source ecosystem around it, so I'm happier than ever that I chose this way.

### 20 years of Visual Studio

Yes, Visual Studio obvsiouly is even older than .NET, and it's getting 20 years old in just a couple of weeks. And this will be the moment when we can finally use an RTM version of Visual Studio 2017. Even the first available beta version was great: lightweight, with a quicker installer, a cleaner configuration and a smaller footprint. After all the RCs we have tried, after all these endless conversions between new JSON project file and the new/old/whatever csproj format, [the final release is coming on March 7](https://launch.visualstudio.com/)! Save the date and prepare to get a never failing and ridiculously fast IDE of your dreams (am I too optimistic?).

As a peek into history, read [a review of the very first Visual Studio from 1997](http://windowsitpro.com/visual-studio/visual-studio-97): VC++5, VB5, and (omg!) Visual SourceSafe... Not that I was programming at this time yet, but still managed to get my share of dreaful experience with this source control monster.

### Update on .NET Core 2.0 release dates

These were all the good news. We were expecting .NET Core 2.0 and .NET Standard 2.0 to be finalized by the same date when VS 2017 would be released. But this week [the new release dates were announced](https://github.com/dotnet/core/blob/master/roadmap.md):

* .NET Core 2.0 Preview: Q2 2017
* .NET Core 2.0: Q3 2017
* .NET Standard 2.0: Q3 2017

```
     .-""""""-.
   .'          '.
  /   O      O   \
 :           `    :
 |                |
 :    .------.    :
  \  '        '  /
   '.          .'
     '-......-'
```

So you still have plenty of time to try to understand [what .NET Standard is](https://andrewlock.net/understanding-net-core-netstandard-and-asp-net-core/), to read about [different types of assemblies in .NET Standard](https://github.com/dotnet/standard/blob/master/docs/history/evolution-of-design-time-assemblies.md) and to play with the [latest update of Visual Studio 2017 RC and (*now try to read slowly and carefully*) .NET Core 1.0 SDK RC4](https://blogs.msdn.microsoft.com/dotnet/2017/02/07/announcing-net-core-tools-updates-in-vs-2017-rc/) (yeah, apparently naming of pre-release versions is even harder than naming your classes).

### NDC London 2017 conference videos available

OK, I lied, these were not *all* the good news. I am happy to announce that several recorded videos of NDC London 2017 talks are [already available on vimeo](https://vimeo.com/channels/1203692) and hopefully they will be adding even more soon. If you are wondering what this NDC is all about, read my impressions about [pre-conference workshops]({{< relref "post/ndc-london-workshop.md" >}}) and [three days of great talks]({{< relref "post/ndc-london-talks.md" >}}).

Are you still here? Just go and learn about [ASP.NET Sockets (a new incarnation of SignalR)](https://vimeo.com/204078084), [a redesigned and rewritten for ASP.NET Core IdentityServer 4](https://vimeo.com/204141878), or [what the reasons behind some design choices of C# were](https://vimeo.com/204075759).