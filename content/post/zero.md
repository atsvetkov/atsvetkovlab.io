---
title: "Never too late"
date: 2016-06-17
tags: ["life", "blogging"]
aliases: ["/2016/06/never-too-late/"]
---

It wasn't until this year that I decided to start blogging. A lot of things changed since moving from Russia to the Netherlands more than a year ago, in both life and work. At my current employer we are undergoing a tough and sometimes painful process of migrating our software architecture and development process to a presumably better state - and I am right in the middle of it. There are plenty discussions and experiments, some of them I hope might be worth sharing with the community.

My initial goal is to write at least one post a week (feel free to poke me if I don't). And I think in the actual first one (*every programmer's posts should be zero-indexed, right?*) I am going to describe the technical parts behind this very blog.  