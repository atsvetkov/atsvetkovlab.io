---
title: "Practice makes perfect"
date: 2016-12-06
tags: ["programming", "exercism", "project euler", "advent of code"]
aliases: ["/2016/12/practice-makes-perfect/"]
---

> Contrary to what you might believe, merely doing your job every day doesn't qualify as real practice. Going to meetings isn't practicing your people skills, and replying to mail isn't practicing your typing. You have to set aside some time once in a while and do focused practice in order to get better at something.

(*Steve Yegge, [Practicing Programming](https://sites.google.com/site/steveyegge2/practicing-programming)*)

I couldn't agree more. More often than not we developers tend to write code only during working hours. It's about eight hours and (hopefully) most of those are spent *actually* programming, so that seems quite enough, right?

Wrong!

<!--more-->

{{< figure src="/img/practice_makes_perfect.jpg" title="" >}}

Well, of course, it depends. We all have lives and keeping the work-life balance is arguably more important than anything else (I am absolutely going to write a separate post about this). But still most great developers became great through constant practice, often outside of working hours. And if you think about it, this is quite normal for other professions, like doctors or musicians. From the same article:

> I have trouble thinking of any modestly difficult profession in which continuous study and practice aren't the norm. Fighter pilots train in simulators before getting into the latest jet. Actors and politicians practice their lines and their smiles. Opera troupes do mock performances before public appearances. Writers, poets, and artists attend workshops, and study the work of the Masters.

> Everyone practices -- everyone, that is, except for us. We just grind stuff out, day in, day out. Are you as embarrassed about the state of our profession as I am?

I always compare it to running. Do you join a competition right away or you practice first? Do you run marathons without training between them? And, in my opinion, all of this is applicable to programming: every minute of focused practice trains your abstract thinking muscle and makes it easier to turn mental models into working code next time you actually need it.

So, how to practice?

* Jeff Atwood has [a blog post exactly about this](https://blog.codinghorror.com/the-ultimate-code-kata/), where he gives some good advice on this matter. Using *coding kata* is a well-known approach to learn TDD, a famous [String Calculator](http://osherove.com/tdd-kata-1/) being one of the most popular. There are [many](http://codekata.com/) [other](https://github.com/garora/TDD-Katas) similar resources, even [a site with code kata video casts](http://www.codekatas.org/casts/tagged/csharp).

* [Exercism](http://www.exercism.io/), an interesting project which allows you to practise in solving programming problems in various popular languages (personally, I started learning [Go](https://golang.org/) on this site some time ago).

* [Project Euler](https://projecteuler.net/) was created in 2001 and is still a very popular source of challenging mathematical/programming problems. These can be quite hard and will require you to brush up your algorithms/data structures skills. I did solve some, but was never too serious about that, which left me with only 17 out of 570 problems finished so far (perhaps I should add the rest to my New Year resolutions).

 {{< figure src="https://projecteuler.net/profile/atsvetkov.png" title="" >}}

* And here's a bonus link, my favourite at the moment: [Advent of Code](http://adventofcode.com/), a set of programming puzzles for each day of December until Christmas. Every day a new one is unlocked, just like in any advent calendar, and the problems themselves are described in a hilarious way, which definitely makes it fun to work on them. Who wouldn't want to help Santa using your programming skills?

{{< figure src="/img/advent_of_code.png" title="" >}}

I only found this site two days ago, so there are already several puzzles to catch up, but this is a really fun way of practicing. Will definitely keep going and encourage you to check it out too.

As Neil Gaiman said, *"you learn by finishing things"*, so just doing one small programming exercise (non work-related!) a day will be of great help on your road to mastery.  