---
title: "Coffee and Links #8"
date: 2019-12-25
tags: ["coffee and links"]
draft: true
---

Algorithms, database design, and coding practice.

<!--more-->

{{< figure src="/img/coffee-and-links-8.jpg" title="" >}}

* Designing Data-Intensive Applications
* HackerRank and LeetCode
* Algorithms course on Coursera
* Python

* plans for 2020Visual Studio 2017 RTM, C# 7.0, and DevOps.